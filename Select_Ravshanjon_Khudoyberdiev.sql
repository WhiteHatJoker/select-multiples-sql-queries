-- Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?

-- VIEWS
CREATE OR REPLACE VIEW RankedSalesPerStore AS
    SELECT
        s.staff_id,
        s.first_name || ' ' || s.last_name AS employee_name,
        st.store_id,
        SUM(p.amount) AS sales_amount,
        ROW_NUMBER() OVER (PARTITION BY st.store_id ORDER BY SUM(p.amount) DESC) AS sales_rank
    FROM staff s
    JOIN payment p ON s.staff_id = p.staff_id
    JOIN rental r ON p.rental_id = r.rental_id
    JOIN inventory i ON r.inventory_id = i.inventory_id
    JOIN store st ON i.store_id = st.store_id
    WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY st.store_id, s.staff_id;
CREATE OR REPLACE VIEW StoreAddress AS
	SELECT 
		s.store_id,
		address, 
		city,
		a.address || ', ' || c.city AS store_location
	FROM store s
	JOIN address a ON s.address_id = a.address_id
	JOIN city c ON a.city_id = c.city_id;

-- SOLUTION 1
SELECT
    employee_name,
    rs.store_id,
    store_location,
    sales_amount
FROM RankedSalesPerStore rs
JOIN staff s ON rs.staff_id = s.staff_id
JOIN StoreAddress st ON rs.store_id = st.store_id
WHERE rs.sales_rank = 1;


-- SOLUTION 2
SELECT
    employee_name,
    s1.store_id,
    store_location,
    sales_amount
FROM RankedSalesPerStore s1
JOIN staff s ON s1.staff_id = s.staff_id
JOIN StoreAddress st ON s1.store_id = st.store_id
WHERE sales_amount = (
    SELECT MAX(sales_amount)
    FROM RankedSalesPerStore s2
    WHERE s1.store_id = s2.store_id
)
ORDER BY sales_amount DESC;

-- Which five movies were rented more than the others, and what is the expected age of the audience for these movies?

-- SOLUTION 1
SELECT 
	f.film_id, 
	f.title, 
	f.rating AS audience_age, 
	COUNT(*) AS rent_count
FROM rental r
JOIN inventory i ON r.inventory_id = i.inventory_id
JOIN film f ON f.film_id = i.film_id
GROUP BY f.film_id, f.title, f.description, f.rating
ORDER BY rent_count DESC LIMIT 5;

-- SOLUTION 2
WITH FilmRentalCounts AS (
	SELECT
		f.film_id,
		f.title,
		f.rating AS audience_age,
		COUNT(*) AS rent_count,
		ROW_NUMBER() OVER (ORDER BY COUNT(*) DESC) AS row_num
	FROM rental r
	JOIN inventory i ON r.inventory_id = i.inventory_id
	JOIN film f ON f.film_id = i.film_id
	GROUP BY f.film_id, f.title, f.rating
)

SELECT
	film_id,
	title,
	audience_age,
	rent_count
FROM FilmRentalCounts
WHERE row_num <= 5;

-- Which actors/actresses didn't act for a longer period of time than the others?
-- I assume it means actors/actresses who didn't act for longer period of time since last film release

-- SOLUTION 1
SELECT 
    a.actor_id,
    a.first_name,
    a.last_name,
    MAX(f.release_year) AS last_film_year,
    EXTRACT(YEAR FROM CURRENT_DATE) - MAX(f.release_year) AS years_inactive_since_last_film
FROM actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON fa.film_id = f.film_id
GROUP BY a.actor_id, a.first_name, a.last_name
HAVING EXTRACT(YEAR FROM CURRENT_DATE) - MAX(f.release_year) >= 5
ORDER BY years_inactive_since_last_film DESC;

-- SOLUTION 2
WITH ActorLastFilmYear AS (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        f.release_year AS last_film_year
    FROM actor a
    JOIN film_actor fa ON a.actor_id = fa.actor_id
    JOIN film f ON fa.film_id = f.film_id
)

SELECT
    actor_id,
    first_name,
    last_name,
    MAX(last_film_year) AS last_film_year,
    EXTRACT(YEAR FROM CURRENT_DATE) - MAX(last_film_year) AS years_inactive_since_last_film
FROM ActorLastFilmYear
GROUP by actor_id, first_name, last_name
HAVING EXTRACT(YEAR FROM CURRENT_DATE) - MAX(last_film_year) >= 5
ORDER by years_inactive_since_last_film DESC;

